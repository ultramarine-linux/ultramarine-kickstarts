%packages

# Appstream data
rpmfusion-*-appstream-data

# Multimedia
gstreamer1-libav
gstreamer1-vaapi
gstreamer1-plugins-bad-freeworld
gstreamer1-plugins-ugly

# Tools
unrar

%end

%post

echo ""
echo "POST BASE REMIX **************************************"
echo ""

# Enable Cisco Open H.264 repository
dnf config-manager --set-enabled fedora-cisco-openh264

%end
