%post

cat > /kickstart.ks << EOF

firstboot --enable
\%post
sed -i 's/^#Current=.*/Current=materia/' /etc/sddm.conf
\%end

EOF

%end