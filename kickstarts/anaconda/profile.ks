%post
#add anaconda config
#ultramarine profile
cat << 'EOF' > /etc/anaconda/product.d/ultramarine.conf
# Anaconda configuration file for Ultramarine Linux

[Product]
product_name = Ultramarine Linux

[Base Product]
product_name = Fedora

[User Interface]
hidden_spokes =
    PasswordSpoke
    UserSpoke
EOF
%end

