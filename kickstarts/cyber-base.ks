%include base.ks
%include cyber-packages.ks

%post


## set default GTK+ theme for root (see #683855, #689070, #808062)
#cat > /root/.gtkrc-2.0 << EOF
#include "/usr/share/themes/Adwaita/gtk-2.0/gtkrc"
#include "/etc/gtk-2.0/gtkrc"
#gtk-theme-name="Adwaita"
#EOF
#mkdir -p /root/.config/gtk-3.0
#cat > /root/.config/gtk-3.0/settings.ini << EOF
#[Settings]
#gtk-theme-name = Adwaita
#EOF

# add initscript
cat >> /etc/rc.d/init.d/livesys << ALLEOF


# Make the welcome screen show up
if [ -f /usr/share/anaconda/gnome/fedora-welcome.desktop ]; then
  mkdir -p ~liveuser/.config/autostart
  cp /usr/share/anaconda/gnome/fedora-welcome.desktop /usr/share/applications/
  cp /usr/share/anaconda/gnome/fedora-welcome.desktop ~liveuser/.config/autostart/
fi


# set up autologin for user liveuser
if [ -f /etc/sddm.conf ]; then
sed -i 's/^#User=.*/User=liveuser/' /etc/sddm.conf
sed -i 's/^#Session=.*/Session=cyber-xsession.desktop/' /etc/sddm.conf
sed -i 's/^#Current=.*/Current=materia/' /etc/sddm.conf
else
cat > /etc/sddm.conf << SDDM_EOF
[Autologin]
User=liveuser
Session=cyber-xsession.desktop
[Theme]
Current=materia
SDDM_EOF
fi

##Configuration
#Create Liveuser dir
mkdir -p /home/liveuser/.config/cyberos
mkdir -p /home/liveuser/.config/
mkdir -p /home/liveuser/Downloads
mkdir -p /home/liveuser/Documents
mkdir -p /home/liveuser/Pictures
mkdir -p /home/liveuser/Videos

#Edit Cyber Configuration
touch /home/liveuser/.config/cyberos/theme.conf
cat << 'EOF' > /home/liveuser/.config/cyberos/theme.conf
[General]
AccentColor=0
DarkMode=false
DarkModeDimsWallpaer=false
PixelRatio=1
Wallpaper=/usr/share/backgrounds/images/default-16_9.png
EOF

#Cyber Dock
touch /home/liveuser/.config/cyberos/dock_pinned.conf
cat << 'EOF' > /home/liveuser/.config/cyberos/dock_pinned.conf

[Firefox]
DesktopPath=/usr/share/applications/firefox.desktop
Exec=firefox
IconName=firefox
Index=0
visibleName=Firefox
 
[cyber-fm]
DesktopPath=/usr/share/applications/cyber-fm.desktop
Exec=cyber-fm
IconName=file-system-manager
Index=2
visibleName=File Manager
 
[cyber-terminal]
DesktopPath=/usr/share/applications/cyber-terminal.desktop
Exec=cyber-terminal
IconName=utilities-terminal
Index=1
visibleName=Terminal
EOF

#Autostart Installer
touch /home/liveuser/.config/autostart/liveinst.desktop
cat << 'EOF' > /home/liveuser/.config/autostart/liveinst.desktop
[Desktop Entry]
Type=Application
Exec=/usr/share/anaconda/gnome/fedora-welcome
Hidden=false
NoDisplay=false
Name=Install Ultramarine Linux
EOF


#Cyber Dock
touch /home/liveuser/.config/cyberos/dock_pinned.conf
cat << 'EOF' > /home/liveuser/.config/cyberos/dock_pinned.conf
[Welcome]
DesktopPath=
Exec=/usr/share/anaconda/gnome/fedora-welcome
IconName=anaconda
Index=2
visibleName=Install Ultramarine
 
[Firefox]
DesktopPath=/usr/share/applications/firefox.desktop
Exec=firefox
IconName=firefox
Index=0
visibleName=Firefox
 
[cyber-fm]
DesktopPath=/usr/share/applications/cyber-fm.desktop
Exec=cyber-fm
IconName=file-system-manager
Index=3
visibleName=File Manager
 
[cyber-terminal]
DesktopPath=/usr/share/applications/cyber-terminal.desktop
Exec=cyber-terminal
IconName=utilities-terminal
Index=1
visibleName=Terminal
EOF



#Set Text Editor for all users
xdg-mime default cyber-edit.desktop text/plain


# make sure to set the right permissions and selinux contexts
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

ALLEOF

##add skeleton home


##Configuration
#Create Liveuser dir
mkdir -p /etc/skel/.config/cyberos
mkdir -p /etc/skel/.config/
mkdir -p /etc/skel/Downloads
mkdir -p /etc/skel/Documents
mkdir -p /etc/skel/Pictures
mkdir -p /etc/skel/Videos

#Edit Cyber Configuration
touch /etc/skel/.config/cyberos/theme.conf
cat << 'EOF' > /etc/skel/.config/cyberos/theme.conf
[General]
AccentColor=0
DarkMode=false
DarkModeDimsWallpaer=false
PixelRatio=1
Wallpaper=/usr/share/backgrounds/images/default-16_9.png
EOF

#Cyber Dock
touch /etc/skel/.config/cyberos/dock_pinned.conf
cat << 'EOF' > /etc/skel/.config/cyberos/dock_pinned.conf

[Firefox]
DesktopPath=/usr/share/applications/firefox.desktop
Exec=firefox
IconName=firefox
Index=0
visibleName=Firefox
 
[cyber-fm]
DesktopPath=/usr/share/applications/cyber-fm.desktop
Exec=cyber-fm
IconName=file-system-manager
Index=2
visibleName=File Manager
 
[cyber-terminal]
DesktopPath=/usr/share/applications/cyber-terminal.desktop
Exec=cyber-terminal
IconName=utilities-terminal
Index=1
visibleName=Terminal
EOF


%end
