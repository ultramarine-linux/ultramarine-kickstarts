
%packages
#install cyber desktop
xorg-x11-server-Xorg
cyber-desktop-common

@core
@standard
@base-x
@input-methods
@dial-up
@multimedia
@fonts
sddm
policycoreutils
coreutils
materia-kde-sddm
sddm-kcm
plasma-discover
qt5ct
plasma-systemsettings




%end
