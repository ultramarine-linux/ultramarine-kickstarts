
%packages
@core
@standard
@base-x
@input-methods
@dial-up
@multimedia
@fonts
policycoreutils
coreutils
plasma-discover
qt5ct
sddm


#install cyber desktop
xorg-x11-server-Xorg
cyber-desktop-common
ultramarine-repos-cyber
meuikit
whitesur-icon-theme-ultramarine


%end