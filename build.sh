#!/bin/bash
spin="${1}"
sudo -s <<<"
rm *.iso
echo Flattening Scripts...
ksflatten --config kickstarts/ultramarine-${spin}.ks --output flattened.ks && sed -i 's/\r$//' flattened.ks
echo ------------------------------------------------------- 
echo --------------------BUILDING ISO-----------------------
echo -------------------------------------------------------
livecd-creator flattened.ks -v --compression-type zstd -f Ultramarine-Linux-Live
"
